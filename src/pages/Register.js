import {useState,useContext,useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';

import Swal from 'sweetalert2';

import UserContext from '../userContext';

import {Navigate} from 'react-router-dom';

export default function Register(){

	const {user,setUser} = useContext(UserContext);

	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [email,setEmail] = useState("");		
	const [mobileNo,setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword,setConfirmPassword] = useState("");

	const [isActive,setisActive] = useState(false);


	useEffect(()=>{
		if(firstName !== "" && lastName !== "" && email !== "" && password !== ""
			&& mobileNo.length === 11  && password === confirmPassword){
			setisActive(true)
		} else{
			setisActive(false)
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword])


	function registerUser(e){
		//prevent submit event's default behavior
		e.preventDefault();

		/*console.log(firstName);
		console.log(lastName);
		console.log(email);
		console.log(mobileNo);
		console.log(password);
		console.log(confirmPassword);*/

		//fetch is a js method allows us to pass/creat to an api.
		//syntax: fectch("<requestURL>",{options});

		fetch('http://localhost:4000/users/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({

				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			//data is the respo0nse of the api/server after it's been processed
			//as JS OBject through our res.json() method.
			console.log(data);
			if(data.email){
				Swal.fire({

					icon:"success",
					title: "Registration Successful",
					text: "Thank you for registering!"
				})

				window.location.href = "/login";

				
			}else{
				Swal.fire({
					icon:"error",
					title: "Registration Failed",
					text: "Something Went Wrong."
				})
			}
		})
	
	}
		

	return (
		
		user.id ? <Navigate to="/courses" replace={true} /> :
		





		<>
			<h1 className="my-5 text-center">Register</h1>
			<Form onSubmit={e =>registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" required value=
					{firstName} onChange={e => {setFirstName(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" required value=
					{lastName} onChange={e =>{setLastName(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" required value=
					{email} onChange={e =>{setEmail(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile No:</Form.Label>
					<Form.Control type="number" placeholder="Enter 11 Digit No." required value=
					{mobileNo} onChange={e =>{setMobileNo(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" required value=
					{password} onChange={e =>{setPassword(e.target.value)}}/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" required value=
					{confirmPassword}onChange={e =>{setConfirmPassword(e.target.value)}}/>
				</Form.Group>
				{
					isActive
					?<Button variant="primary" type="submit" className="my-5">Submit</Button>
					:<Button variant="primary" disabled>Submit</Button>
				}

			</Form>
		</>
		
		)
}