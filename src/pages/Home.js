import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){

	

	//let sampleProp = "I am sample data passed from Home component to Banner component."

	//props are data we can pass from a parent component to a child component

	//Parent components are components which return other components.

	//Child components are components returned by a parent component.


	//The name of the atrtribute will become the name of a property of the object that
	//the child component recieves. THat's whey it's called props. Props stand for properties.

	let sampleProp2 = "This sample data is passed from Home to Highlights component."


	let bannerData = {

		title: "Zuitt Booking System B152",
		description: "View and book a course from our catalog!",
		buttonText: "View Our Courses",
		destination:"/courses"

	}
	

	return (

			<>
				<Banner bannerProp={bannerData}/>
				<Highlights HighlightsProp={sampleProp2}/>
			</>

	)


}