//if you're using states, import it first 
import {useState} from 'react'

import {Button,Card} from 'react-bootstrap';

import {Link} from 'react-router-dom'


export default function CourseCard({courseProp}){

	//console.log(courseProp);

	console.log("Hello, I will run whenever we update a state with its setter function.")

	const [count,setCount] = useState(0);

	//useState react hook returns an array that contains the state and the setter funtion
	//count was destructured from our useState()and initial value is the argument
	//added to useState(<intialValue>)
	//console.log(count);
	//console.log(useState("Hello"));

//states
// they are ways to store information within a component. advantage from a variable
//is that a variable do not retain updated information when a component is updated.

//Creating a state: 

//useState() hook from react will allow us to create a state and its setter function

//let seat = 0;

	const [seats,setSeats] = useState(30);


	




	function enroll(){
		setCount(count + 1);
		//seat++
		setSeats(seats - 1);


	}



	//console.log(courseProp);


	return(
			<Card>
				<Card.Body>
					<Card.Title>
						{courseProp.name}
					</Card.Title>
					<Card.Text>
						{courseProp.description}
					</Card.Text>
					<Card.Text>
						Price: {courseProp.price}
					</Card.Text>
					<Link to={`/courses/viewCourse/${courseProp._id}`} className="btn btn-primary">View Course</Link>

					
					
				</Card.Body>
			</Card>
	)

}











