

//react-bootstrap components are react components that create/return react
//elements with the appropriate bootstrap classes.
import{Button, Row, Col} from 'react-bootstrap'

//destructure the special object recieved and get only the props we need.
export default function Banner({bannerProp}){

	console.log(bannerProp);

	


	return (
		
		<Row>
			<Col className="p-5">
				<h1 className="mb-3">{bannerProp.title}</h1>
				<p className="my-3">{bannerProp.description}</p>
				<a href={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonText}</a>
			</Col>
			
		</Row>
		)


}