import {Row,Col,Card} from 'react-bootstrap';

export default function Highlights({HighlightsProp}){


	console.log(HighlightsProp)

	return (


			<Row className="my-3">
				<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
						<Card.Body>
							<Card.Title>
								<h2>Learn From Home</h2>
							</Card.Title>
							<Card.Text>
								Incididunt incididunt in exercitation.
							</Card.Text>

						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
						<Card.Body>
							<Card.Title>
								<h2>Study Now, Pay Later</h2>
							</Card.Title>
							<Card.Text>
								Lorem ipsum dolor laborum mollit ut labore in veniam
								 dolor ex ex deserunt incididunt aute enim enim mollit 
								 magna amet laboris nulla magna ex officia voluptate 
								 incididunt eiusmod commodo.
							</Card.Text>
							
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="p-3 cardHighlight">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
								Nisi ut officia dolore mollit labore anim ut 
								excepteur dolor incididunt ut irure eiusmod aliqua 
								commodo aute aliqua. Cillum velit dolore non voluptate 
								cupidatat non enim excepteur nostrud fugiat tempor 
								exercitation mollit minim nulla.
							</Card.Text>
							
						</Card.Body>
					</Card>
				</Col>
			</Row>

		)



}