let coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum in in fugiat.",
		price: 25000,
		onOffer:true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Esse duis reprehenderit velit cillum pariatur ut fugiat consequat.",
		price: 35000,
		onOffer:true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Labore cupidatat velit.",
		price: 45000,
		onOffer:true
	},
	{
		id: "wdc004",
		name: "React.js",
		description: "Mollit sed ad proident sint incididunt amet quis ullamco adipisicing non ex duis aliqua pariatur labore.",
		price: 40000,
		onOffer:false
	}


];

export default coursesData;